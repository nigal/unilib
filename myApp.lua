local centroX=display.contentCenterX
local centroY=display.contentCenterY

local M = {}


--Funzione sfondo azzurrino
local function sfondo(r,g,b)
	local bg=display.newRect(centroX,centroY,display.contentWidth,display.contentHeight)
	bg:setFillColor(r/255,g/255,b/255)
	return bg
end
M.sfondo=sfondo


return M


