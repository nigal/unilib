local composer = require ("composer")
local myApp=require("myApp")
local timer=require("timer")

local splash=display.newImage("splash.png",display.contentCenterX,display.contentCenterY)

timer.performWithDelay(500,function()
	display.remove(splash)
end)
composer.gotoScene("menu",{effect = "fade", time=500})
